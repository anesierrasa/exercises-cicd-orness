from sqlalchemy import text


def test_db(app):
    with app.db.connect() as conn:
        # Prepare the DB and data
        conn.execute(
            text(
                """CREATE TABLE client
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    prenom VARCHAR(100),
    nom VARCHAR(100),
    ville VARCHAR(255),
    age INT
)"""
            )
        )
        conn.execute(
            text(
                """INSERT INTO client (prenom, nom, ville, age) VALUES
 ('Rébecca', 'Armand', 'Saint-Didier-des-Bois', 24),
 ('Aimée', 'Hebert', 'Marigny-le-Châtel', 36),
 ('Marielle', 'Ribeiro', 'Maillères', 27),
 ('Hilaire', 'Savary', 'Conie-Molitard', 58)"""
            )
        )

        # Search for any data
        result = conn.execute(text("""SELECT * FROM client"""))
        result = [row._asdict() for row in result.fetchall()]

    expected = [
        {"id": 1, "prenom": "Rébecca", "nom":"Armand", "ville": "Saint-Didier-des-Bois", "age": 24},
        {"id": 2,"prenom": "Aimée", "nom":"Hebert", "ville": "Marigny-le-Châtel", "age": 36},
        {"id": 3,"prenom": "Marielle", "nom":"Ribeiro", "ville": "Maillères", "age": 27},
        {"id": 4,"prenom": "Hilaire", "nom":"Savary", "ville": "Conie-Molitard", "age": 58},
    ]
    assert result == expected
